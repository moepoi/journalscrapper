from flask import Flask, request, jsonify
from scrapper.gscholar import getCitations, getCitation
from scrapper.sinta import getUsers, getUser

app = Flask(__name__)

# Disable Auto Sort key in Jsonify
app.config.update(
    JSON_SORT_KEYS = False,
)

@app.route('/')
def home():
    return jsonify({'status': 'ok'})

@app.route('/getusers')
def get_users():
    name = request.args['name']
    data = getUsers(name)
    return jsonify(data)

@app.route('/getuser')
def get_user():
    user_id = request.args['id']
    data = getUser(user_id)
    return jsonify(data)

@app.route('/getcitations')
def get_citations():
    user_id = request.args['id']
    data = getCitations(user_id)
    return jsonify(data)

@app.route('/getcitation')
def get_citation():
    citation_id = request.args['id']
    data = getCitation(citation_id)
    return jsonify(data)

if __name__ == '__main__':
    app.run(threaded=True, host="0.0.0.0", debug=True)